#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'httparty'

class Gitlab
  include HTTParty
  base_uri 'gitlab.com/api/v4'

  User = Struct.new(:id, :username, :name)
  MergeRequest = Struct.new(:id, :title, :author)

  def initialize(access_token)
    @access_token = access_token
  end

  def find_user(username)
    response = get('/users', username: username)
    result = response.parsed_response.first
    return unless result

    parse_user(result)
  end

  def merge_requests_assigned_to_user(user_id)
    response = get(
      '/merge_requests',
      scope: 'all', state: 'opened', assignee_id: user_id, per_page: 100
    )

    response.parsed_response.map do |mr|
      MergeRequest.new(
        mr.fetch('id'),
        mr.fetch('title'),
        parse_user(mr.fetch('author'))
      )
    end
  end

  private

  def parse_user(params)
    User.new(
      params.fetch('id'),
      params.fetch('username'),
      params.fetch('name')
    )
  end

  def get(path, query)
    options = { headers: headers, query: query }
    self.class.get(path, options)
  end

  def headers
    { 'Private-Token' => @access_token }
  end
end

class ReviewersAvailable
  attr_reader :usernames, :gitlab

  def initialize(access_token, usernames)
    @gitlab = Gitlab.new(access_token)
    @usernames = usernames

    @results = review_counts_by_user.sort_by { |_user, count| count }
  end

  def print
    print_table(@results)
  end

  private

  def print_table(results)
    column_width = 20
    header1 = 'USER'.ljust(column_width)
    header2 = 'ASSIGNED REVIEWS'
    puts "#{header1} #{header2}"

    results.each do |user, count|
      puts "#{user.name.ljust(column_width)} #{count}"
    end
  end

  def review_counts_by_user
    user_and_reviews = usernames.map do |username|
      user = gitlab.find_user(username)
      next unless user

      reviews = reviews_for_user(user)

      [user, reviews.count]
    end.compact

    Hash[user_and_reviews]
  end

  def reviews_for_user(user)
    merge_requests = gitlab.merge_requests_assigned_to_user(user.id)

    merge_requests.reject do |merge_request|
      merge_request.author == user
    end
  end
end

DEFAULT_CONFIG_FILE = '~/.reviewers_available.yml'

config_file ||= ARGV[0] || DEFAULT_CONFIG_FILE
config_file = File.expand_path(config_file)

abort "Config file #{config_file} does not exist" unless File.exists?(config_file)

config = YAML.load_file(config_file)
access_token = config.fetch('access_token')
usernames = config.fetch('usernames')

ReviewersAvailable.new(access_token, usernames).print
