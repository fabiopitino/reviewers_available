# Reviewers available

This is a script that taking a list of GitLab usernames fetches the number of merge requests assigned to the user for review (where the assignee is not the author).

## Installation

```bash
curl https://gitlab.com/fabiopitino/reviewers_available/-/raw/master/reviewers_available.rb \
  -o ~/.bin/reviewers_available

chmod +x ~/.bin/reviewers_available

gem install httparty
```

Create a file in your home folder called `~/.reviewers_available.yml`, alternatively you can pass a config file as command line argument.

```yaml
usernames:
  - fabiopitino
  - ayufan
  - grzesiek

access_token: your-api-access-token
```

## Usage

```bash
reviewers_available
# USER                 ASSIGNED REVIEWS
# Fabio Pitino         8
# Grzegorz Bizon       10
# Kamil Trzciński      13

# Alternatively you can pass a config file as command line argument
reviewers_available config.yml
```
